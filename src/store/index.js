import Vue from 'vue'
import Vuex from "vuex"
import a from './modules/a.js';

Vue.use(Vuex)

const store = new Vuex.Store({
  state:{
    counter:0,
    stu:[
      {name: 'kk', age: 20},
      {name: 'bb', age: 31},
      {name: 'cc', age: 10},
    ],
    info: {
      name: 'kitty',
      sex: '1',
      fav: 'football'
    }
  },
  mutations:{
    incr(state) {
      state.counter+=1
    },
    decr(state) {
      state.counter-=1
    },
    incrCount(state, payload) {
      state.counter+=payload.addtionCount
    },
    decrCount(state, payload) {
      state.counter-=payload.addtionCount
    },
    addStudent(state, payload) {
      //var stu = state.stu
      state.stu.push(payload.stu)
    },
    editInfoData(state, payload) {
      console.log(payload);
      var {nickname} = payload


      Vue.set(state.info, 'nickname', nickname)
    }
    // editInfoData(state, payload) {
    //   console.log(payload);
    //   // var {name} = payload.info


    //   // Vue.set(state.info, 'nickname', name)
    // }
  },
  getters: {
    getLess20(state) {
      return state.stu.filter(item => item.age < 20)
    },
    getLessAge(state) {
      return (age) => {
        return state.stu.filter(item => item.age < age)
      }
    },
    getStuCount(state, getters) {
      return (age) => {
        return getters.getLessAge(age).length
      }
    }
  },
  actions: {
    aEditInfo: function(context, payload) {
      console.log(context);
      setTimeout(() => {
        context.commit('editInfoData', payload)
      }, 2000);
    }
  },
  modules: {
    a:a
  }
})


export default store
