export default {
  state: {
    play1: {
      name: 'mario',
      age: 20
    },
    play2: {
      name: 'ezio',
      age: 31
    },
    players: [
      {
        name: 'bk',
        age: 20
      },
      {
        name: 'rdo',
        age: 34
      }
    ]
  },
  mutations: {
    editPlay1Name(state, playload) {
      state.play1.name = playload.name
    }
  },
  getters: {
    playerLess30(state, getters, rootState, rootGetters) {
      console.log(state);
      console.log(getters);
      console.log(rootState);
      console.log(rootGetters);
      return state.players.filter((item) => item.age <= 30)
    }
  },
  actions: {
    MaEditPlay1(context, playload) {
      console.log(context);
      setTimeout(function() {
        context.commit('editPlay1Name', playload)
      }, 1000)
    }
  }
};